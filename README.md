# backend

## Scripts

### `npm start`

Starts a local development server (using your computer as a host).
Use this command within git from within project folder


### `npm test`

Runs the standard linter and the tests if they exist. Will run on pipeline.

### `npm standardize`

Standardizes the code with the standard linter.

## Dev Environment

### Tools

* Git
* NodeJS
* NPM
* Express
* Standard
* PostgreSQL
* Gitlab-CI

### .env File

Create a file `.env` in the backend folder containing these definitions:

```
DB_USER=''
DB_PASS=''
```

You should fill in your credentials for your DB.

### Windows

#### Git

Git is the tool used for version control for this project. Git can be installed on windows by simply downloading and running the executable from [here](https://git-scm.com/download/win).

After installation it can be checked in the command line with:

```
git -v
```

#### NodeJS & NPM

NodeJS and NPM come with each other. On Windows this is a simple install. Simply download and run the stable version installer of NodeJS from [here](https://nodejs.org/en/). The stable one always has an even number before the first dot.

After they are installed you should be able to confirm the installation by entering a command line and entering:

```
npm -v
```

and

```
node -v
```

#### Node Packages

Open the backend folder. In a terminal, this can be done by being in the folder above and entering `cd ./backend/`.

To install everything else you will need to run the command `npm install`.

#### PostgreSQL

Download and install [PostgreSQL](https://www.postgresql.org/) for Windows.

You will have to set a password for your superuser. Please write this down for later.

After installing, restart your PC.

With PostreSQL now installed, run pgAdmin.

Enter your passwords and in the top right corner click the run button, this will allow you to run queries.

Copy the contents of `./postgres/table-schema.sql` into the query box and run it. This should create your table.

#### Starting the project

Congratz! you have the entire dev environment installed!

You can now start the project with the script `npm start` or use `npm run dev` to start the dev environment (automatically reloads the project when source code is changed).

