const { createActivities } = require('../src/service/database')

const activities = [
  {
    title: 'Marble Run with Play-doh',
    materials: 'Marble(s), Play dough, Cups, Tape, Cookie/Baking sheet',
    description: 'This activity allows kids to use their imagination, by creating a path or track for a marble to run down. Within this activity there are two version, one for younger kids and another for older kids.',
    instructions: 'For younger kids, start by giving them a little demonstration on how the marble rolls down the cooking sheet. ' +
      'Then use the Play-doh and show them how they can alter the path. Then let them use their imagination to create their own path. ' +
      'For other kids, explain what the over idea is. Explain to them that they can use the cups, tape, and Play-doh to create a track for the marble to role down on. ' +
      'Maybe challenge them to make a really complex track or a one that starts off really high.',
    timeEstimate: 30,
    imgURL: 'https://i.imgur.com/RzymaIp.png',
    tags: ['Creative', 'DIY', 'Track', 'Marble run', 'Play-doh']
  },
  {
    title: 'Straw Bridge Challenge',
    materials: 'Straws, Scotch tape, Paper cup, 200-300 pennies, A support structure (such as   two tables), Paperclips',
    description: 'This activity challenges kids to use their imagination and whatever engineering skills they have, to create a bridge out of just straws and scotch tape',
    instructions: 'The goal is to create a bridge using a limited amount of straws and scotch tape(parents can decide how much). ' +
      'Then place the kid(s) bridge design on top and in between to desks or tables that are at the same height. Reference the image above for clarification. ' +
      'Then use the paper cup and the paperclips to hang the cup from the kid(s) bridge. Continue to drop pennies into the paper cup until the bridge fails/collapses',
    timeEstimate: 60,
    imgURL: 'https://www.mybaba.com/wp-content/uploads/2016/05/straw.jpg',
    tags: ['Creative', 'Challenge', 'Indoor', 'Engineer', 'Straws']
  },
  {
    title: 'Marshmallow Challenge',
    materials: 'Marshmallows, Uncooked spaghetti noodles, Masking tape (or tape of your choosing',
    description: 'This activity is meant to challenge the kid(s) participating. The objective is to construct a tower as high as possible using only spaghetti and masking tape. ' +
      'The marshmallow must be placed on top of the tower. If there are multiple kids participating, then they can play against each other. ' +
      'If there is just one kid, then you can challenge them to keep redesigning and build higher and higher',
    instructions: 'As stated earlier the goal is to build the largest tower possible using only uncooked spaghetti noodles and tape. ' +
      'The marshmallow must be placed on top. To make this activity more challenging, you can limit how many pieces of spaghetti they get as well as tape',
    timeEstimate: 30,
    imgURL: 'http://blog.explo.org/hubfs/yale/academics/slideshows/20180711_170730/6.jpg',
    tags: ['Creative', 'Challenge', 'Indoor', 'Marshmallow', 'DIY']
  },
  {
    title: 'Laundry Basket Skee-ball',
    materials: '2-3 laundry baskets or similarly large containers, Cereal box, Cardboard box, Duct Tape, balls safe for indoor use, Paper, Markers, Scissors',
    description: 'This activity allows kids to play skee-ball right at home. Its simple to set up and it’s something active to play indoors that will (hopefully) not involve breaking anything!  And it’s not very loud either',
    instructions: 'Create a ramp for the balls to roll up on using the cardboard box and the cereal box. The cardboard box will act as the main structure for the ramp, while the cereal box is used to smooth out the ramp part itself. ' +
      'The use of duct tape to hole the boxes together is recommended, however other forms of tape or glue could be used. ' +
      'Create a point system for each basket you have using the paper and the markers. Then tape the point system to the laundry baskets. ' +
      'Demonstrate to your kids how it works once or twice. To make it more difficult for older kids, the laundry baskets could be moved farther back',
    timeEstimate: 90,
    imgURL: 'https://1.bp.blogspot.com/-esvuQFG4PDk/WSRm6LsTdMI/AAAAAAAAFi8/sBcjy94SZlk4U1heL1sdQxSh9mvOkLEygCLcB/s1600/DSC_0796.JPG',
    tags: ['Exercise', 'Skee ball', 'Skee', 'DIY']
  },
  {
    title: 'Recycled Art',
    materials: 'Washable paint, Markers, Recyclable materials (Cardboard boxes and/or tubes, egg cartons, bubble wrap, tape), General art supplies',
    description: 'This activity can be used to distract or keep your kids busy for a few hours. Help them gather miscellaneous trash or recyclables from around the house. ' +
      'Also gather any art supplies you may have. Then you can either have your kid(s) create whatever they want and have them come up with their own ideas. ' +
      'Or you can give assistance and ask them to make something specific, like a flower or a robot',
    instructions: 'Allow your kid(s) to create whatever they can think of using their own imagination. ' +
      'Another idea is challenge them yourself by asking them to make something specific',
    timeEstimate: 60,
    imgURL: 'http://supplies.thesmartteacher.com.s3.amazonaws.com/assets/exchange/DSC_0020.JPG',
    tags: ['Art', 'Creative', 'DIY']
  }
]

const insert = async () => {
  for (let i = 0; i < activities.length; i++) {
    await createActivities(activities[i])
  }
}
insert()
