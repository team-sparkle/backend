DROP TABLE IF EXISTS activities;
DROP TABLE IF EXISTS activity_tags;
DROP TABLE IF EXISTS activities_tags_rel;
DROP TABLE IF EXISTS users;

--timeEstimate/dateCreated in double quotes so they can use camelCase
CREATE TABLE IF NOT EXISTS activities(
    id SERIAL,
    title VARCHAR(255),
    materials TEXT,
    description TEXT,
    instructions TEXT,
    likes INT DEFAULT 0,
    dislikes INT DEFAULT 0,
    "imgURL" TEXT,
    "timeEstimate" INT,
    "dateCreated" TIMESTAMP NOT NULL DEFAULT NOW(),
    PRIMARY KEY(id)
);

CREATE TABLE IF NOT EXISTS activity_tags(
    id SERIAL,
    tag VARCHAR(255),
    PRIMARY KEY(id)
);

--Represents relationship between activity and tag
CREATE TABLE IF NOT EXISTS activities_tags_rel (
    activities_id INT,
    activity_tags_id INT
);

CREATE TABLE IF NOT EXISTS users (
    id SERIAL,
    username TEXT,
    name TEXT,
    password TEXT,
    PRIMARY KEY(id)
);
