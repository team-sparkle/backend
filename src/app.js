const express = require('express')
const cors = require('cors')
const DatabaseService = require('./service/database')
const jwt = require('jsonwebtoken')

const app = express()

app.use(express.json())
app.use(cors())

app.get('/', (req, res) => {
  res.status(200).send('Hello World!')
})

app.get('/activities', (req, res) => {
  DatabaseService.getActivities()
    .then(result => res.status(200).send(result))
    .catch(err => res.status(500).send(err))
})

app.post('/new-activity', async (req, res) => {
  try {
    const result = await DatabaseService.createActivities(req.body)

    res.status(201).json(result)
  } catch (err) {
    console.error(err)
    res.status(500).send(err)
  }
})

app.post('/like', (req, res) => {
  const { id } = req.body
  DatabaseService.updateLike(id)
    .then(result => res.status(200).send(result))
    .catch(err => res.status(500).send(err))
})

app.post('/dislike', (req, res) => {
  const { id } = req.body
  DatabaseService.updateDislike(id)
    .then(result => res.status(200).send(result))
    .catch(err => res.status(500).send(err))
})

app.post('/login', async function (req, res, next) {
  const { username, password } = req.body
  DatabaseService.getUser(username, password).then(user => {
    if (user !== null && user !== undefined) {
      const token = jwt.sign({
        username: user.username,
        name: user.name,
        id: user.id
      }, 'User logged in.', { expiresIn: 28800000 })
      res.status(200).json({
        success: true,
        err: null,
        token
      })
    } else {
      res.status(401).json({
        success: false,
        err: `User ${username} does not exist or invalid password was entered!`,
        token: null
      })
    }
  }).catch(err => res.status(500).send(err))
})

module.exports = app
