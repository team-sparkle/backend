const format = require('pg-format')
const pg = require('pg')
require('dotenv').config()

const config = {
  user: process.env.DB_USER,
  password: process.env.DB_PASS,
  database: 'postgres',
  port: 5432, // For docker
  max: 10
}

const pool = new pg.Pool(config)

const getActivities = () => {
  const query = `
        SELECT act.id,
               act.title,
               act.materials,
               act.description,
               act.instructions,
               act."imgURL",
               act."timeEstimate",
               act."dateCreated",
               act."likes",
               act."dislikes",
               case
                   when count(activity_tags) = 0 then '[]'
                   else json_agg(json_build_object('id', activity_tags.id, 'tag', activity_tags.tag)) end AS tags
        FROM activities AS act
                 LEFT JOIN public.activities_tags_rel rel on act.id = rel.activities_id
                 LEFT JOIN activity_tags ON rel.activity_tags_id = activity_tags.id
        GROUP BY act.id;
       `
  return pool.query(query)
    .then(({ rows }) => rows)
}

// Insert tags for activities
const insertActivityTags = async (client, tags = [], activityId) => {
  const queryTags = tags.map((tag) => [tag])
  if (queryTags.length === 0) {
    return []
  }

  const getIdsFromExistingTagsQuery = `
      SELECT tags.id, input.tag
      FROM activity_tags AS tags
               RIGHT JOIN (VALUES %L) AS input(tag) ON input.tag = tags.tag;
  `

  // object containing all tags with and without ids assigned
  const mixedIds = await client.query(format(getIdsFromExistingTagsQuery, queryTags))
    .then(({ rows }) => rows)

  const newTags = mixedIds
    .filter(({ id }) => !id)
    .map(({ tag }) => [tag])

  let newTagsWithIds = []
  if (newTags.length !== 0) {
    const insertNewTagsQuery = 'INSERT INTO activity_tags (tag) VALUES %L RETURNING *;'

    // tags w/out ids that now have an id
    newTagsWithIds = await client.query(format(insertNewTagsQuery, newTags))
      .then(({ rows }) => rows)
  }

  // Object containing all tags
  const allTags = mixedIds.filter(({ id }) => id).concat(newTagsWithIds)

  const activityTagsRelationshipQuery = 'INSERT INTO activities_tags_rel (activities_id, activity_tags_id) VALUES %L'
  await client.query(format(activityTagsRelationshipQuery, allTags.map(({ id }) => [activityId, id])))

  return allTags
}

const createActivities = async ({ title, materials, description, instructions, timeEstimate, tags, imgURL }) => {
  const activitiesQuery = `INSERT INTO activities 
  (title, materials, description, instructions, "timeEstimate", "imgURL") 
  VALUES ($1, $2, $3, $4, $5, $6) RETURNING *`

  const client = await pool.connect()

  const activity = await client.query(activitiesQuery, [title, materials, description, instructions, timeEstimate, imgURL])
    .then(({ rows }) => rows[0])

  const allTags = await insertActivityTags(client, tags, activity.id)
  client.release()

  // Combine the activity and tags that are now saved in the db and return them
  return {
    ...activity,
    tags: allTags
  }
}

const getUser = async (username, password) => {
  // takes in user and pass, queries users table where name=name and pass=pass return username, name, id
  const userQuery = `SELECT id, username, name 
                       FROM users
                       WHERE users.username = $1
                       AND users.password = $2`

  const client = await pool.connect()

  const user = await client.query(userQuery, [username, password])
    .then(({ rows }) => {
      if (rows.length > 0) {
        return rows[0]
      }
      return null
    })
  return user
}

const updateDislike = async (id) => {
  const dislikeQuery = `UPDATE activities
                     SET dislikes = activities.dislikes + 1
                     WHERE id = $1
                     RETURNING dislikes`

  const client = await pool.connect()

  return client.query(dislikeQuery, [id])
    .then((results) => results.rows[0])
}

const updateLike = async (id) => {
  const likeQuery = `UPDATE activities
                     SET likes = activities.likes + 1
                     WHERE id = $1
                     RETURNING likes`

  const client = await pool.connect()

  return client.query(likeQuery, [id])
    .then((results) => results.rows[0])
}

module.exports = {
  getActivities,
  createActivities,
  getUser,
  updateLike,
  updateDislike
}
