#!/bin/bash

NAME=kidsfort-db
export DB_PASS=pass
export DB_USER=postgres

echo "Removing any old $NAME containers";
docker rm -f $NAME;

echo;
echo "Creating $NAME postgresql image";
docker run --name $NAME -v $(pwd)/postgres/:/docker-entrypoint-initdb.d -e POSTGRES_PASSWORD=$DB_PASS -p 5433:5432 -d postgres;
