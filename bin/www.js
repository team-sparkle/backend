#!/usr/bin/env node

const app = require('../src/app')

const port = 8080

app.listen(port, () => {
  console.log(`listening on port ${port}`)
})
